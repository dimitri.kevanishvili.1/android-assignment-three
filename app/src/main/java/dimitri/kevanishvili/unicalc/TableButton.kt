package dimitri.kevanishvili.unicalc

data class TableButton(var text: String, var type: String);

package dimitri.kevanishvili.unicalc


object References {
    val TABLE_BUTTONS: Array<Array<TableButton>> = arrayOf(
        arrayOf(
            TableButton("%", "specOp"),
            TableButton("√", "specOp"),
            TableButton("+/-", "specOp"),
        ),
        arrayOf(
            TableButton("7", "number"),
            TableButton("8", "number"),
            TableButton("9", "number"),
            TableButton("*", "operator")
        ),
        arrayOf(
            TableButton("4", "number"),
            TableButton("5", "number"),
            TableButton("6", "number"),
            TableButton("-", "operator")
        ),
        arrayOf(
            TableButton("1", "number"),
            TableButton("2", "number"),
            TableButton("3", "number"),
            TableButton("+", "operator")
        ),
        arrayOf(
            TableButton("C", "clear"),
            TableButton("0", "number"),
            TableButton(".", "number"),
            TableButton("/", "operator")
        ),
        arrayOf(
            TableButton("=", "specOp")
        )
    );
}

package dimitri.kevanishvili.unicalc

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.widget.Button
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlin.math.floor
import kotlin.math.sqrt


class MainActivity : AppCompatActivity() {
    private lateinit var resultTextView: TextView
    private lateinit var tableLayout: TableLayout;
    private var clearNextClick: Boolean = false;
    private var operand: Double = 0.0;
    private var operation: String = "";

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        resultTextView = findViewById(R.id.tv_result)
        tableLayout = findViewById(R.id.tbl_lay);
        arrangeButtonRows()
        clearResult();
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun arrangeButtonRows() {
        val buttonParams = TableRow.LayoutParams(
            TableRow.LayoutParams.WRAP_CONTENT,
            (60 * resources.displayMetrics.density).toInt(), 1f
        )
        val rowParams = TableLayout.LayoutParams(
            TableLayout.LayoutParams.MATCH_PARENT,
            (40 * resources.displayMetrics.density).toInt()
        )
        for ((rowIndex, row) in References.TABLE_BUTTONS.withIndex()) {
            val tableRow = TableRow(this);
            tableRow.layoutParams = rowParams
            tableRow.weightSum = row.size.toFloat();
            for ((buttonIndex, button) in row.withIndex()) {
                val buttonElement = Button(this);
                buttonElement.layoutParams = buttonParams
                buttonElement.background =
                    resources.getDrawable(R.drawable.button_bg_op, theme)
                when (button.type) {
                    "number" -> {
                        buttonElement.background =
                            resources.getDrawable(R.drawable.button_bg_number, theme)
                        buttonElement.setOnClickListener {
                            numberClick(button.text)
                        }
                    }
                    "operator" -> {
                        buttonElement.setTextColor(Color.WHITE)
                        buttonElement.setOnClickListener {
                            operationClick(button.text)
                        }
                    }
                    "specOp" -> {
                        buttonElement.setTextColor(Color.WHITE)
                        buttonElement.setOnClickListener {
                            specialOperationClick(button.text)
                        }
                    }
                    "clear" -> {
                        buttonElement.setTextColor(Color.WHITE)
                        buttonElement.setOnClickListener {
                            clearClick()
                        }
                    }
                }
                buttonElement.text = button.text;
                tableRow.addView(buttonElement, buttonIndex);
            }
            tableLayout.addView(tableRow, rowIndex);
        }
    }

    private fun clearResult() {
        resultTextView.text = "";
    }

    private fun clearClick() {
        clearResult();
        operand = 0.0;
        operation = "";
    }

    private fun getOperandFromInput(): Double {
        val operandText = resultTextView.text.toString();
        if (operandText.isNotEmpty()) {
            return operandText.toDouble();
        }
        return 0.0
    }

    private fun setOperandAndText(result: Double, setOperand: Boolean = true) {
        var resultText = result.toString();
        if ((result == floor(result)) && !result.isInfinite()) {
            resultText = resultText.replace(".0", "");
        }
        resultTextView.text = resultText;
        if (setOperand) operand = result;
    }

    private fun numberClick(buttonNum: String) {
        if (clearNextClick) {
            clearResult();
            clearNextClick = false
        }
        var text = resultTextView.text.toString();
        if (text == "0") {
            text = ""
        }
        if (text.contains('.') and (buttonNum == ".")) {
            return
        }
        val result = text + buttonNum;
        resultTextView.text = result
    }

    private fun specialOperationClick(op: String) {
        when (op) {
            "+/-" -> {
                val result = if (operation.isEmpty()) {
                    getOperandFromInput()
                } else {
                    operand
                }
                setOperandAndText(result * -1)
            }
            "√" -> {
                val result = if (operation.isEmpty()) {
                    getOperandFromInput()
                } else {
                    operand
                }
                setOperandAndText(sqrt(result))
            }
            "%" -> {
                val second = getOperandFromInput()
                val firstOperand =
                    if (operand == 0.0) 1.0 else operand // Hope this doesn't fuck it up...
                val result = (second / 100) * firstOperand;
                setOperandAndText(result, false)
            }
            "=" -> {
                if (operation.isEmpty()) return
                equalsClick();
                operation = ""
            }
        }
    }

    private fun operationClick(op: String) {
        if (operation.isEmpty()) {
            operand = getOperandFromInput()
            clearResult();
        } else {
            equalsClick()
        }
        operation = op
    }

    private fun equalsClick() {
        val secondOperand = getOperandFromInput();
        var result = 0.0;
        when (this.operation) {
            "+" -> result = (operand + secondOperand)
            "-" -> result = (operand - secondOperand)
            "/" -> result = (operand / secondOperand)
            "*" -> result = (operand * secondOperand)
        }
        setOperandAndText(result)
        clearNextClick = true
    }
}